<?php

/**
 * Classe permettant de gérer les [user:*] 
 */
class UserManager implements Manager 
{
    use SingletonTrait;

    /**
     * Methode principale qui permet de traiter la gestion de tous les tag de type [user:]
     * @param string $text
     * @param User $user
     * @return string text
     */ 
    public function computeText($text, $user)
    {   
        if ($user instanceof User){
            $text = $this->getFirstName($text, '[user:first_name]', ucfirst(mb_strtolower($user->firstname)));
        }
        return $text;
    }

    /**
     * Methode qui permet de re remplace le tag choisi dans le text 
     * @param string $text
     * @param string $key
     * @param string $value
     * @return string $text
     */ 
    public function singleFormat($text, $key, $value)
    {
        $iskey = strpos($text, $key);
        if ($iskey !== false) {
            $text = str_replace(
                $key, 
                $value, 
                $text
            );
        }
        return $text;
    }

    /**
     * @param string $text
     * @param string $userTag
     * @param string $value
     * @return string $text
     */
    private function getFirstName($text, $userTag, $value)
    {
        return $this->singleFormat($text, $userTag, $value);
    }
    
}
