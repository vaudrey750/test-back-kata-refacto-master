<?php

interface Manager
{
    public function computeText($text, $modificator);
    public function singleFormat($test, $key, $value);
}
