<?php

/**
 * Classe permettant de gérer les [quote:*] 
 */
class QuoteManager implements Manager 
{
    use SingletonTrait;

    /**
     * Methode principale qui permet de traiter la gestion de tous les tag de type [quote:]
     * @param string $text
     * @param Quote $quote
     * @return string $text
     */ 
    public function computeText($text, $quote)
    {
        if ($quote instanceof Quote)
        {
            $_quoteFromRepository = QuoteRepository::getInstance()->getById($quote->id);
            $usefulObject = SiteRepository::getInstance()->getById($quote->siteId);
            $destinationOfQuote = DestinationRepository::getInstance()->getById($quote->destinationId);
            
            $text = $this->getSummaryHtml($text, '[quote:summary_html]', $_quoteFromRepository);
            $text = $this->getSummaryText($text, '[quote:summary]', $_quoteFromRepository);

            $text = $this->getDestinationLink($text, '[quote:destination_link]', 
                                                $usefulObject->url,
                                                $destinationOfQuote->countryName,
                                                $_quoteFromRepository->id
                                            );
            $text = $this->getDestinationName($text, '[quote:destination_name]', $destinationOfQuote->countryName);
        }
        return $text;
    }
    
    /**
     * Methode qui permet de re remplace le tag choisi dans le text 
     * @param string $text
     * @param string $key
     * @param string $type
     * @return string $text
     */ 
    public function formatValue($text, $key, $type, Quote $quote){
        $iskey = strpos($text, $key);
        if ($iskey !== false) {
            $text = str_replace(
                $key,
                Quote::render($quote, $type),
                $text
            );
        }
        return $text;
    }

    /**
     * Methode qui permet de re remplace le tag choisi dans le text 
     * @param string $text
     * @param string $key
     * @param string $value
     * @return string $text
     */
    public function singleFormat($text, $key, $value)
    {
        $iskey = strpos($text, $key);
        if ($iskey !== false) {
            $text = str_replace(
                $key, 
                $value, 
                $text
            );
        }
        return $text;
    }

    /**
     * @param string $text
     * @param string $tag
     * @param Quote $quote
     * @return string $text
     */
    private function getSummaryHtml($text, $tag, Quote $quote)
    {
        return $this->formatValue($text, $tag, 'html', $quote);
    }

    /**
     * @param string $text
     * @param string $tag
     * @param Quote $quote
     * @return string $text
     */
    private function getSummaryText($text, $tag, Quote $quote)
    {
        return $this->formatValue($text, $tag, 'text', $quote);
    }

    /**
     * @param string $text
     * @param string $tag
     * @param string $url
     * @param string $countryName
     * @param integer $quoteId
     * @return string $text
     */
    private function getDestinationLink($text, $tag, $url, $countryName, $quoteId)
    {
        $value = $url . '/' . $countryName . '/quote/' . $quoteId;
        return $this->singleFormat($text, $tag, $value);
    }

    /**
     * @param string $text
     * @param string $tag
     * @param string $countryName
     * @return string $text
     */
    private function getDestinationName($text, $tag, $countryName)
    {
        return $this->singleFormat($text, $tag, $countryName);
    }
    
}
