<?php

class TemplateManager
{

    protected $app_context;

    public function __construct()
    {
        //Rechercher le context de l'application
        $this->app_context = ApplicationContext::getInstance();
    }

    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);
        $replaced->subject = $this->computeText($replaced->subject, $data);
        $replaced->content = $this->computeText($replaced->content, $data);

        return $replaced;
    }

    private function computeText($text, array $data)
    {   
        //Remplacer les [quote:*] par les bonnes valeurs contenue dans la data["quote"]
        $quote = (isset($data['quote']) and $data['quote'] instanceof Quote) ? $data['quote'] : null;
        $text = QuoteManager::getInstance()->computeText($text, $quote);

        //Remplacer les [userte:*] par les bonnes valeurs contenue dans la data["user"]
        $user = (isset($data['user'])  and ($data['user']  instanceof User))  ? $data['user']  : $this->app_context->getCurrentUser();
        $text = UserManager::getInstance()->computeText($text, $user);

        return $text;
    }
}
